#!/bin/bash
cat "$1" | while read username
do
    if [ -n "$username" ];then
       echo $username
    else
       exit 1
    fi
done
